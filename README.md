# KeynoteTest

Animations iincluded in demo:
* [x] Add calendar event and confirmation
* [x] Loading Bar
* [x] Multiple spinners
* [x] Furniture screen: Checklist with third party images and ability to select an item 
     * [x] navigate to selected item's product detail screen
* [x] Login screen

## Video Walkthrough

Here's a walkthrough of different animations:

![ ](walkthrough.gif)

### Discoveries
- There are no gestures in Keynote (for example: pinch, scroll, long press, etc), everything is a click/tap
